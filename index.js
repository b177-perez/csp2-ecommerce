const express = require("express");
const mongoose = require("mongoose");

// Allows our backend application to be available to our frontend application
const cors = require("cors");

// Allows access to routes defined within the application
const userRoutes = require("./routes/user");
const productRoutes = require("./routes/product");

const app = express();


// Connect to our MongoDB database

mongoose.connect("mongodb+srv://camille_admin:admin@cluster0.msjya.mongodb.net/csp2-ecommerce?retryWrites=true&w=majority", {

	useNewUrlParser: true,
	useUnifiedTopology: true

});


// Prompts a message for successful database connection

mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas'));


// Allows all resources to access the backend application

app.use(cors());

app.use(express.json());
app.use(express.urlencoded({extended:true}));


// Defines the "/users" string to be included for all user routes defined in the "user" route file
app.use("/users", userRoutes);

// Defines the "/products" string to be included for all product routes in the "product" route file
app.use("/products", productRoutes);


// App listening to port 4001

app.listen(process.env.PORT || 4001, () => {
	console.log(`API is now online on port ${ process.env.PORT || 4001 }`)
});

