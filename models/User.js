const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({

	email : {
		type : String,
		required : [true, "Email is required"]
	},
	password : {
		type : String,
		required : [true, "Password is required"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	createdOn : {
		type : Date,
		default : new Date()
	},
	orders : [
		{
			productId : {
				type : String,
				required: [true, "Product Id is required"]
			},
			purchasedOn : {
				type : Date,
				default : new Date()
			}

		}	
	]

})


module.exports = mongoose.model("User", userSchema);
