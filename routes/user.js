const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");
const productController = require("../controllers/product");
const auth = require("../auth");


// Route checking if the user's email already exists in the database
// endpoint: localhost:4001/users/checkEmail

router.post("/checkEmail", (req, res) => {

	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));

});


// Route for registering a user.
// endpoint: localhost:4001/users/register

router.post("/register", (req, res) => {
	
	console.log(req.body);
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});



// Route for user authentication.
// endpoint: localhost:4001/users/login

router.post("/login", (req, res) => {

	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});


// Route for updating users as admin by an Admin
// endpoint: localhost:4001/users/:userId/setAsAdmin

router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {

	const userData = {

        user : req.body,
        isAdmin : auth.decode(req.headers.authorization).isAdmin
    }

    console.log(userData);

    userController.updateUserAsAdmin(req.params, userData).then(resultFromController => res.send(resultFromController))
});



// Route for Creating Order by A Non-Admin
// endpoint: localhost:4001/users/checkout

router.post("/checkout", auth.verify, (req, res) => {

    const data = {
        userId : req.body.userId,
        productId : req.body.productId,
		quantity : req.body.quantity,     
        isAdmin : auth.decode(req.headers.authorization).isAdmin
    }

	userController.createOrder(data).then(resultFromController => res.send(resultFromController));
})



// Route for retrieving ALL Orders by An Admin
// endpoint: localhost:4001/users/orders

router.get("/orders", auth.verify, (req, res) => {

	const data = {
		userId : auth.decode(req.headers.authorization).id,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	console.log(data);

	userController.getAllOrders(data).then(resultFromController => res.send(resultFromController));
});



// Route for retrieving My Orders by A Non-Admin
// endpoint: localhost:4001/users/myOrders

router.get("/myOrders", auth.verify, (req, res) => {

	const data = {
		userId : auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	console.log(data);

	userController.getMyOrders(data).then(resultFromController => res.send(resultFromController));
});




module.exports = router;
