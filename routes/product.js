const express = require("express");
const router = express.Router();
const productController = require("../controllers/product");
const auth = require("../auth");


// Route for creating a Product by an Admin
// endpoint: localhost:4001/products

router.post("/", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};

	console.log(data);

	productController.addProduct(data).then(resultFromController => res.send(resultFromController));
});


// Route for retrieving ALL Active Products
// endpoint: localhost:4001/products

router.get("/", (req, res) => {

	productController.getAllActiveProducts().then(resultFromController => res.send(resultFromController));
});


// Route for retrieving specific product
// endpoint: localhost:4001/products/:productId

router.get("/:productId", (req, res) => {

	console.log(req.params.productId);

	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});



// Route for updating a product by an Admin
// endpoint: localhost:4001/products/:productId

router.put("/:productId", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	console.log(data);

	productController.updateProduct(req.params, data).then(resultFromController => res.send(resultFromController));
});



// Route for archiving a product by an Admin
// endpoint: localhost:4001/products/:productId/archive

router.put("/:productId/archive", auth.verify, (req, res) =>{

    const data = {

        product : req.body,
        isAdmin : auth.decode(req.headers.authorization).isAdmin
    };

    console.log(data);

    productController.archiveProduct(req.params, data).then(resultFromController => res.send(resultFromController))
});


module.exports = router;
