// imports the Product model
const Product = require("../models/Product");

// imports the Order model
const Order = require("../models/Order");

// imports the User model
// const User = require("../models/User");

// import bcrypt - for encrypting password
const bcrypt = require("bcrypt");

// import jwt - user authentication
const auth = require("../auth");



// Controller function for creating a Product by an Admin

module.exports.addProduct = (data) => {

	// User is an admin - can create Product
	if (data.isAdmin) {

		// Creates a variable "newProduct" and instantiates a new "Product" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information

		let newProduct = new Product({
			name : data.product.name,
			description : data.product.description,
			price : data.product.price
		});

		// Saves the created object to our database
		return newProduct.save().then((product, error) => {

			// Product creation failed
			if (error) {

				return false + " - Error in product creation.";

			// Product creation successful
			} else {

				return true + " - New product successfully created.";

			};

		});

	// User is not an admin - cannot create Product
	} else {
		return Promise.resolve(false + " - Only Admin user can create a product.");
	};
	

};



// Controller function for getting ALL Active Products 

module.exports.getAllActiveProducts = () => {
	return Product.find({ isActive : true }).then(result => {
		return result;
	})
}



// Controller function for retrieving a specific product

module.exports.getProduct = (reqParams) => {

	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
}



// Controller function for updating a product by an Admin

module.exports.updateProduct = (reqParams, data) => {

    if(data.isAdmin) {

        let updatedProduct = {

            name : data.product.name,
			description : data.product.description,
			price : data.product.price

        }

        return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
            
            if(error){
                return false + " - Error in updating product info.";
            }
            else{
                return true + " - Product info successfully updated.";
            }
        })
    }
    // Non-admin cannot update a product
    else{
        return Promise.resolve(false + " - Only Admin user can update a product info.");
    }
}



// Controller function for archiving a product by an Admin

module.exports.archiveProduct = (reqParams, data) => {

    if(data.isAdmin) {

        let archivedProduct = {

            isActive : false
        }

        return Product.findByIdAndUpdate(reqParams.productId, archivedProduct).then((product, error) => {
            
            if(error){
                return false + " - Error in archiving a product.";
            }
            else{
                return true + " - Product archived successfully.";
            }
        })
    }
    // Non-admin cannot archive a product
    else{
        return Promise.resolve(false + " - Only Admin user can archive a product.");
    }
}

