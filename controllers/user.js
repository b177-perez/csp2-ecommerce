// imports the User model
const User = require("../models/User");

// imports the Product model
const Product = require("../models/Product");

// imports the Order model
const Order = require("../models/Order");

// import bcrypt - for encrypting password
const bcrypt = require("bcrypt");

// import jwt - user authentication
const auth = require("../auth");


// Controller function for checking email duplicates

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email : reqBody.email}).then(result => {
		if(result.length > 0){
			return true + " - Email exists.";
		}
		else{
			return false + " - Email doesn't exist.";
		}
	});
};


// Controller function for user registration

module.exports.registerUser = (reqBody) => {

	// Creates variable "newUser" and instantiates a new "User" object using the mongoose model

	let newUser = new User({
		email : reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10)
	});


	// Saves the created object to our database

	return newUser.save().then((user, error) => {

		// User registration failed
		if(error){
			return false + " - User registration failed.";
		}

		// User registration successful
		else{
			return true + " - User registration successful.";
		}

	});

};


// User authentication (/login)

module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {

		// User does not exist
		if(result == null){
			return false + " - User doesn't exist.";
		}

		// User exists
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect){

				// Generate an access token
				return { access : auth.createAccessToken(result)}
			}

			// Passwords do not match
			else{
				return false + " - Wrong credentials.";
			}
		}
	});
};



// Admin can set users as admin

module.exports.updateUserAsAdmin = (reqParams, data) => {

    if(data.isAdmin) {

        let updatedUserAsAdmin = {

            isAdmin : true
        };

        return User.findByIdAndUpdate(reqParams.userId, updatedUserAsAdmin).then((user, error) => {
            
            if(error){
                return false + " - Error updating user info.";
            }
            else{
                return true + " - User info updated successfully.";
            }
        })
    }

    // Non-Admin - cannot update
    else{
        return Promise.resolve(false + " - You cannot update user info.");
    }
}



module.exports.createOrder = async (data) => {

	// User is an admin - Cannot create Order
	if (data.isAdmin) {

		return Promise.resolve(false + " - Admin users are not allowed to create an order.");

	// User is not an admin - Can create Order
	} else {
		
		let userCreateOrder = await Product.findById(data.productId).then(product =>{
            if(product.isActive == true){
                
                let newOrder = new Order({
                    userId: data.userId,
                    productId: data.productId,
                    quantity: data.quantity,
                    totalAmount: product.price * data.quantity
                })

                return newOrder.save().then((product, error) => {
                    if(error){
                        return false + " - Error in saving Order.";
                    }
                    else {
                        return true + " - Order creation successful.";
                    }
                })
            }
            else {
                return false + " - Product is not available.";
            }
        })

        let isProductUpdated = await Product.findById(data.productId).then(product =>{
            if(product.isActive == true){
                product.orders.push({userId : data.userId})

                return product.save().then((product, error) => {
                    if(error){
                        return false + " - Error in saving Product order.";
                    }
                    else{
                        return true + " - Product order successfully saved.";
                    }
                })
            }
            else {
                return false + " - Product is not available.";
            }
        })

        let isUserUpdated = await User.findById(data.userId).then(user =>{
            user.orders.push({productId : data.productId})
 
            return user.save().then((user, error) => {
                 if(error){
                     return false + " - Error in saving User order.";
                 }
                 else{
                     return true + " - User order successfully saved.";
                 }
             })
         })

         if(userCreateOrder && isProductUpdated && isUserUpdated){
            return true + " Order is processed successfully."
         }
         else {
            return false + " Error in processing order."
         }

	};


};



// Controller function for getting ALL Orders

module.exports.getAllOrders = (data) => {


	if(data.isAdmin) {

		return Order.find({}).then(result => {
			return result;
		})

	}

	// Non-Admin Cannot Retrieve ALL Orders
	else{
		return Promise.resolve(false + " - Only Admin users can retrieve all orders.");
	}
	
}



module.exports.getMyOrders = (data) => {

	if(data.isAdmin) {

		return Promise.resolve(false + " - Admin users cannot use this functionality.");

	}

	// Non-Admin Can Retrieve My Orders
	else{
		
		return Order.find({ userId : data.userId}).then(result => {
			return result;
		})

	}
	
}


